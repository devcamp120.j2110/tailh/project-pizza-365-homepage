import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css";
import ContentComponent from "./Components/ContentComponents/ContentComponent";
import FooterComponent from "./Components/FooterComponent";
import HeaderComponent from "./Components/HeaderComponent";

function App() {
  return (
    <div>
      {/* Header */}
      <HeaderComponent/>
      {/* Content */}
      <ContentComponent/>
      {/* Footer */}
      <FooterComponent/>
    </div>
  );
}

export default App;
