import { useState, useEffect } from "react";
import DrinkSelect from "./DrinkSelect/DrinkSelect";
import FormSubmit from "./FormSubmit/FormSubmit";
import Info from "./Info/Info";
import Lading from "./Lading/Lading";
import PizzaSize from "./PizzaSize/PizzaSize";
import PizzaType from "./PizzaType/PizzaType";
import SubmitButton from "./SubmitButton/SubmitButton";

function ContentComponent() {
  // Pizza Size
  const [pizzaSize, setPizzaSize] = useState("");
  const changePizzaSize = (paramPizzaCombo) => {
    setPizzaSize(paramPizzaCombo);
  };
  // Pizza Type
  const [pizzaType, setPizzaType] = useState("");
  const changePizzaType = (paramPizzaType) => {
    setPizzaType(paramPizzaType);
  };
  // Drink Select
  const [drinkSelect, setDrinkSelect] = useState("");
  const changeDrinkSelect = (paramDrinkSelect) => {
    setDrinkSelect(paramDrinkSelect);
  };

  //Input Name
  const [inputName, setInputName] = useState("");
  const changeInputName = (paramInputName) => {
    setInputName(paramInputName);
  };

  //Input Email
  const [inputEmail, setInputEmail] = useState("");
  const changeInputEmail = (paramInputEmail) => {
    setInputEmail(paramInputEmail);
  };

  //Input Phone Number
  const [inputPhone, setInputPhone] = useState("");
  const changeInputPhone = (paramInputPhone) => {
    setInputPhone(paramInputPhone);
  };

  //Input Address
  const [inputAddress, setInputAddress] = useState("");
  const changeInputAddress = (paramInputAddress) => {
    setInputAddress(paramInputAddress);
  };

  //Input Voucher
  const [inputVoucher, setInputVoucher] = useState("");
  const changeInputVoucher = (paramInputVoucher) => {
    setInputVoucher(paramInputVoucher);
  };

  //Input Message
  const [inputMessage, setInputMessage] = useState("");
  const changeInputMessage = (paramInputMessage) => {
    setInputMessage(paramInputMessage);
  };

  useEffect(() => {
    console.log(pizzaSize === "" ? `PizzaSize: ${pizzaSize}` : pizzaSize);
    console.log(`Pizza Type: ${pizzaType}`);
    console.log(drinkSelect === "" ? `Drink: ${drinkSelect}` : drinkSelect);
    console.log(`Name: ${inputName}`);
    console.log(`Email: ${inputEmail}`);
    console.log(`Phone: ${inputPhone}`);
    console.log(`Address: ${inputAddress}`);
    console.log(`Voucher: ${inputVoucher}`);
    console.log(`Message: ${inputMessage}`);
  });

  return (
    <div
      className="container"
      style={{ marginTop: "56px", marginBottom: "40px" }}
    >
      <h1 className="fw-bold pt-5 color-main">PIZZA 365</h1>
      <h5 className="fst-italic fw-bold color-main">Truly iatalian!</h5>
      <Lading />
      <Info />
      <PizzaSize pizzaSize={pizzaSize} changePizzaSize={changePizzaSize} />
      <PizzaType pizzaType={pizzaType} changePizzaType={changePizzaType} />
      <DrinkSelect changeDrinkSelect={changeDrinkSelect} />
      <FormSubmit
        inputName={inputName}
        changeInputName={changeInputName}
        
        inputEmail={inputEmail}
        changeInputEmail={changeInputEmail}
        
        inputPhone={inputPhone}
        changeInputPhone={changeInputPhone}
        
        inputAddress={inputAddress}
        changeInputAddress={changeInputAddress}
        
        inputVoucher={inputVoucher}
        changeInputVoucher={changeInputVoucher}
        
        inputMessage={inputMessage}
        changeInputMessage={changeInputMessage}
        
      />
      <SubmitButton
        pizzaSize={pizzaSize}
        pizzaType={pizzaType}
        drinkSelect={drinkSelect}
        inputName={inputName}
        inputEmail={inputEmail}
        inputPhone={inputPhone}
        inputAddress={inputAddress}
        inputVoucher={inputVoucher}
        inputMessage={inputMessage}

        changeInputVoucher={changeInputVoucher}
      />
    </div>
  );
}

export default ContentComponent;
