import { Button } from "reactstrap";

function PizzaSize({ pizzaSize, changePizzaSize }) {
  const pizzaSmallChoosed = () => {
    console.log("Pizza Small Choosed");
    var pizzaSizeConstructor = {
      kichCo: "S",
      duongKinh: "20",
      suon: "2",
      salad: "200",
      soLuongNuoc: "2",
      thanhTien: "150000",
    };
    // Thay đổi state pizzaSize
    changePizzaSize(pizzaSizeConstructor);
  };
  const pizzaMediumChoosed = () => {
    console.log("Pizza Medium Choosed");
    var pizzaSizeConstructor = {
      kichCo: "M",
      duongKinh: "25",
      suon: "4",
      salad: "300",
      soLuongNuoc: "3",
      thanhTien: "200000",
    };
    // Thay đổi state pizzaSize
    changePizzaSize(pizzaSizeConstructor);
  };
  const pizzaLargeChoosed = () => {
    console.log("Pizza Large Choosed");
    var pizzaSizeConstructor = {
      kichCo: "L",
      duongKinh: "30",
      suon: "8",
      salad: "500",
      soLuongNuoc: "4",
      thanhTien: "250000",
    };
    // Thay đổi state pizzaSize
    changePizzaSize(pizzaSizeConstructor);
  };

  return (
    <div className="type-combo-description">
      <div className="content-title mb-4 d-flex justify-content-center flex-column">
        <h2 id="size-pizza" className="text-center">
          <span>Chọn size pizza</span>
        </h2>
        <h5 className="text-center color-main p-1">
          Chọn combo pizza phù hợp với nhu cầu của bạn
        </h5>
      </div>
      <div className="row justify-content-between p-0 m-0">
        <div className="col-sm-4 pl-0">
          <div className="card text-center">
            <div className="card-header">
              <h3 className="fw-bold">S (small)</h3>
            </div>
            <div className="card-body">
              <p className="card-text">
                Đường Kính: <span>20cm</span>
              </p>
              <p className="card-text">
                Sườn Nướng: <span>2</span>
              </p>
              <p className="card-text">
                Salad: <span>200g</span>
              </p>
              <p className="card-text">
                Nước Ngọt: <span>2</span>
              </p>
              <h4 className="card-title fw-bold">
                <span>150.000</span>
              </h4>
              <p className="card-text">VNĐ</p>
            </div>
            <div className="card-footer">
              <Button
                id="btn-combo-small"
                color={pizzaSize.kichCo === "S"? "danger" : "warning"}
                onClick={pizzaSmallChoosed}
              >
                Chọn
              </Button>
            </div>
          </div>
        </div>
        <div className="col-sm-4">
          <div className="card text-center">
            <div className="card-header">
              <h3 className="fw-bold">M (medium)</h3>
            </div>
            <div className="card-body">
              <p className="card-text">
                Đường Kính: <span>25cm</span>
              </p>
              <p className="card-text">
                Sườn Nướng: <span>4</span>
              </p>
              <p className="card-text">
                Salad: <span>300g</span>
              </p>
              <p className="card-text">
                Nước Ngọt: <span>3</span>
              </p>
              <h4 className="card-title fw-bold">
                <span>200.000</span>
              </h4>
              <p className="card-text">VNĐ</p>
            </div>
            <div className="card-footer">
              <Button
                onClick={pizzaMediumChoosed}
                id="btn-combo-medium"
                className="btn btn-warning"
                color={pizzaSize.kichCo === "M" ? "danger" : "warning"}
              >
                Chọn
              </Button>
            </div>
          </div>
        </div>
        <div className="col-sm-4 pr-0">
          <div className="card text-center">
            <div className="card-header">
              <h3 className="fw-bold">L (large)</h3>
            </div>
            <div className="card-body">
              <p className="card-text">
                Đường Kính: <span>30cm</span>
              </p>
              <p className="card-text">
                Sườn Nướng: <span>8</span>
              </p>
              <p className="card-text">
                Salad: <span>500g</span>
              </p>
              <p className="card-text">
                Nước Ngọt: <span>4</span>
              </p>
              <h4 className="card-title fw-bold">
                <span>250.000</span>
              </h4>
              <p className="card-text">VNĐ</p>
            </div>
            <div className="card-footer">
              <Button
                onClick={pizzaLargeChoosed}
                id="btn-combo-big"
                color={pizzaSize.kichCo === "L" ? "danger" : "warning"}
              >
                Chọn
              </Button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default PizzaSize;
