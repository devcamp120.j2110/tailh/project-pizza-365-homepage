import slide1 from "../../../assets/images/1.jpg";
import slide2 from "../../../assets/images/2.jpg";
import slide3 from "../../../assets/images/3.jpg";
import slide4 from "../../../assets/images/4.jpg";
import { Carousel } from "react-bootstrap";

function Lading() {
  return (
    <Carousel>
      <Carousel.Item>
        <img className="d-block w-100" src={slide1} alt="1 slide" />
      </Carousel.Item>
      <Carousel.Item>
        <img className="d-block w-100" src={slide2} alt="2 slide" />
      </Carousel.Item>
      <Carousel.Item>
        <img className="d-block w-100" src={slide3} alt="3 slide" />
      </Carousel.Item>
      <Carousel.Item>
        <img className="d-block w-100" src={slide4} alt="4 slide" />
      </Carousel.Item>
    </Carousel>
  );
}

export default Lading;
