import imgPizzaType1 from "../../../assets/images/seafood.jpg";
import imgPizzaType2 from "../../../assets/images/hawaiian.jpg";
import imgPizzaType3 from "../../../assets/images/bacon.jpg";
import { Button } from "reactstrap";


function PizzaType({ pizzaType, changePizzaType }) {
  const pizzaTypeSeafoodChoosed = () => {
    // Thay đổi state pizzaType
    changePizzaType("Pizza Seafood");
  };
  const pizzaTypeHawaiiChoosed = () => {
    // Thay đổi state pizzaType
    changePizzaType("Pizza Hawaii");
  };
  const pizzaTypeBaconChoosed = () => {
    // Thay đổi state pizzaType
    changePizzaType("Pizza Bacon");
  };

  return (
    <div className="pizza-type">
      <div className="content-title d-flex justify-content-center">
        <h2 className="text-center">Chọn Loại Pizza</h2>
      </div>
      <div className="container row">
        <div className="col-sm-4">
          <div className="card align-items-center">
            <img src={imgPizzaType1} className="card-img-top" alt="..."></img>
            <div className="card-body mb-2">
              <h5 className="card-title fw-bold">OCEAN MANIA</h5>
              <p className="card-text">PIZZA HẢI SẢN SỐT MAYONNAISE</p>
              <p>
                Xốt Cà Chua , Phô Mai Mozzarella, Tôm, Mực,Thanh Cua, Hành Tây.
              </p>
              <Button
                id="btn-type-seafood"
                className="fw-bold "
                style={{ width: "100%" }}
                onClick={pizzaTypeSeafoodChoosed}
                color={pizzaType === "Pizza Seafood" ? "danger" : "warning"}
              >
                Chọn
              </Button>
            </div>
          </div>
        </div>
        <div className="col-sm-4">
          <div className="card align-items-center">
            <img src={imgPizzaType2} className="card-img-top" alt="..."></img>
            <div className="card-body mb-2">
              <h5 className="card-title fw-bold">HAWAIIAN</h5>
              <p className="card-text">PIZZA DĂM BÔNG DỨA KIỂU HAWAII</p>
              <p>Xốt Cà Chua , Phô Mai Mozzarella, Thịt Dăm Bông, Thơm.</p>
              <Button
                id="btn-type-hawaii"
                className="fw-bold "
                style={{ width: "100%" }}
                onClick={pizzaTypeHawaiiChoosed}
                color={pizzaType === "Pizza Hawaii" ? "danger" : "warning"}
              >
                Chọn
              </Button>
            </div>
          </div>
        </div>
        <div className="col-sm-4">
          <div className="card align-items-center">
            <img src={imgPizzaType3} className="card-img-top" alt="..."></img>
            <div className="card-body mb-2">
              <h5 className="card-title fw-bold">CHEESY CHICKEN BACON</h5>
              <p className="card-text">PIZZA GÀ PHÔ MAI THỊT HEO XÔNG KHÓI</p>
              <p>
                Xốt Phô Mai, Thịt Gà, Thịt Heo Muối, Phô Mai Mozzarella, Cà
                Chua.
              </p>
              <Button
                id="btn-type-bacon"
                className="fw-bold "
                style={{ width: "100%" }}
                onClick={pizzaTypeBaconChoosed}
                color={pizzaType === "Pizza Bacon" ? "danger" : "warning"}
              >
                Chọn
              </Button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default PizzaType;
