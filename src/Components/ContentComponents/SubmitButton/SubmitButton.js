import { Button, Container, FormGroup, Label, Col, Input } from "reactstrap";
import { Modal } from "react-bootstrap";
import { useState } from "react";

function SubmitButton({
  pizzaSize,
  pizzaType,
  drinkSelect,
  inputName,
  inputEmail,
  inputPhone,
  inputAddress,
  inputVoucher,
  inputMessage,
  changeInputVoucher,
}) {
  const fetchApi = async (paramUrl, paramOption = {}) => {
    const response = await fetch(paramUrl, paramOption);
    const responseData = await response.json();
    return responseData;
  };

  const [showModalOrder, setShowModalOrder] = useState(false);
  const handleCloseModalOrder = () => setShowModalOrder(false);
  const handleShowModalOrder = () => setShowModalOrder(true);

  const [showModalError, setShowModalError] = useState(false);
  const handleCloseModalError = () => setShowModalError(false);
  const handleShowModalError = () => setShowModalError(true);

  const [showModalSuccess, setShowModalSuccess] = useState(false);
  const handleCloseModalSuccess = () => setShowModalSuccess(false);
  const handleShowModalSuccess = () => setShowModalSuccess(true);

  //State Lưu giá trị báo lỗi
  const [errorMessage, setErrorMessage] = useState("");

  //State lưu giá trị vào Input ở Modal với initValue là Prop inputVoucher
  const [modalInputVoucher, setModalInputVoucher] = useState(inputVoucher);
  //State lưu phần trăm giảm giá
  const [percent, setPercent] = useState("");

  //State orderId
  const [orderId, setOrderId] = useState("");

  //Check Voucher ID
  const checkVoucherById = () => {
    console.log("Check Voucher");
    fetchApi(
      `http://42.115.221.44:8080/devcamp-voucher-api/voucher_detail/${inputVoucher.trim()}`
    )
      .then((data) => {
        changeInputVoucher(data.maVoucher); //Thay Đổi lại State ở component cha
        setModalInputVoucher(data.maVoucher); //Thay Đổi ở Input Modal
        setPercent(data.phanTramGiamGia); // Thay Đổi giá trị percent
      })
      .catch((err) => {
        console.log(err);
        changeInputVoucher("");
        setModalInputVoucher("Không Tìm Thấy Mã Giảm Giá");
      });
  };
  //Tính Toán Thành Tiền khi có voucher
  const getTotalPrice = (paramPrice, paramPercent) => {
    let total = "";
    total = paramPrice - (paramPrice * paramPercent) / 100;
    return total;
  };
  //Xử lý sự kiện khi nhấn nút gửi Đơn
  const handleUserData = () => {
    var vValidated = validateData();
    if (vValidated) {
      handleShowModalOrder();
    }
  };
  //Xử lý sự kiện khi nhấn nút Save
  const callApiCreateOrder = () => {
    //TH nhập đúng voucher tính lại tổntg tiền
    var total = "";
    if (modalInputVoucher !== "") {
      total = getTotalPrice(pizzaSize.thanhTien, percent);
    } else {
      total = pizzaSize.thanhTien;
    }
    let vObjectRequest = {
      kichCo: pizzaSize.kichCo,
      duongKinh: pizzaSize.duongKinh,
      suon: pizzaSize.suon,
      salad: pizzaSize.salad,
      loaiPizza: pizzaType,
      idVourcher: inputVoucher,
      idLoaiNuocUong: drinkSelect.drinkValue,
      soLuongNuoc: pizzaSize.soLuongNuoc,
      hoTen: inputName,
      thanhTien: total,
      email: inputEmail,
      soDienThoai: inputPhone,
      diaChi: inputAddress,
      loiNhan: inputMessage,
    };
    const order = {
      method: "POST",
      body: JSON.stringify(vObjectRequest),
      headers: {
        "Content-type": "application/json; charset=UTF-8",
      },
    };
    fetchApi("http://42.115.221.44:8080/devcamp-pizza365/orders", order)
      .then((data) => {
        console.log(data);
        setOrderId(data.orderId);
      })
      .catch((err) => {
        handleCloseModalOrder();
        console.log(err);
      })
      .finally(() => {
        handleCloseModalOrder();
        handleShowModalSuccess();
      });
  };
  //Validate Dữ liệu
  const validateData = () => {
    if (pizzaSize === "") {
      setErrorMessage("Chưa Chọn Size Pizza");
      handleShowModalError();
      return false;
    }
    if (pizzaType === "") {
      setErrorMessage("Chưa Chọn Loại Pizza");
      handleShowModalError();
      return false;
    }
    if (drinkSelect.drinkValue === "") {
      setErrorMessage("Chưa Chọn Nước Uống");
      handleShowModalError();
      return false;
    }
    if (inputName === "") {
      setErrorMessage("Chưa Nhập Tên");
      handleShowModalError();
      return false;
    }
    if (inputEmail === "") {
      setErrorMessage("Chưa Nhập Email");
      handleShowModalError();
      return false;
    }
    //Kiểm tra email theo chuẩn
    var vEmailRegex = /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/;
    if (!vEmailRegex.test(inputEmail)) {
      setErrorMessage("Email Nhập Chưa Đúng Chuẩn");
      handleShowModalError();
      return false;
    }

    if (inputPhone === "") {
      setErrorMessage("Chưa Nhập Số Điện Thoại");
      handleShowModalError();
      return false;
    }
    if (isNaN(inputPhone)) {
      setErrorMessage("Số Điện Thoại Nhập Chưa Đúng");
      handleShowModalError();
      return false;
    }
    if (inputAddress === "") {
      setErrorMessage("Chưa Nhập Địa Chỉ");
      handleShowModalError();
      return false;
    }

    //TH Người Dùng Có Nhập vào inputVoucher
    if (inputVoucher.trim() !== "") {
      checkVoucherById();
    }

    //TH Người Dùng Không Nhập vào inputVoucher
    if (inputVoucher.trim() === "") {
      setModalInputVoucher("");
    }
    return true;
  };

  //Reset All Data
  const resetData = () => {
    handleCloseModalSuccess();
    //RS trang sau khi ẩn modal
    setTimeout(() => {
      window.location.reload();
    }, 500);
  };
  return (
    <Container>
      <Button
        type="submit"
        id="btn-save-data"
        className="btn btn-warning fw-bold"
        style={{ width: "100%" }}
        onClick={handleUserData}
      >
        Gửi
      </Button>
      {/* Order Modal */}
      <Modal show={showModalOrder} onHide={handleCloseModalOrder} size="lg">
        <Modal.Header closeButton>
          <Modal.Title>Order Info</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {/* Tên */}
          <FormGroup row>
            <Label for="" sm={2}>
              Họ Tên:
            </Label>
            <Col sm={10}>
              <Input value={inputName} readOnly={true} />
            </Col>
          </FormGroup>
          {/* Email */}
          <FormGroup row>
            <Label for="" sm={2}>
              Email:
            </Label>
            <Col sm={10}>
              <Input value={inputEmail} readOnly={true} />
            </Col>
          </FormGroup>
          {/* Số Điện Thoại */}
          <FormGroup row>
            <Label for="" sm={2}>
              Số Điện Thoại:
            </Label>
            <Col sm={10}>
              <Input value={inputPhone} readOnly={true} />
            </Col>
          </FormGroup>
          {/* Địa Chỉ */}
          <FormGroup row>
            <Label for="" sm={2}>
              Địa Chỉ:
            </Label>
            <Col sm={10}>
              <Input value={inputAddress} readOnly={true} />
            </Col>
          </FormGroup>

          {/* Kích Cỡ */}
          <FormGroup row>
            <Label for="" sm={2}>
              Kích Cỡ Pizza:
            </Label>
            <Col sm={10}>
              <Input value={pizzaSize.kichCo} readOnly={true} />
            </Col>
          </FormGroup>

          {/* Loại Pizza */}
          <FormGroup row>
            <Label for="" sm={2}>
              Loại Pizza:
            </Label>
            <Col sm={10}>
              <Input value={pizzaType} readOnly={true} />
            </Col>
          </FormGroup>

          {/* Nước Uống */}
          <FormGroup row>
            <Label for="" sm={2}>
              Nước Uống :
            </Label>
            <Col sm={10}>
              <Input value={drinkSelect.drinkText} readOnly={true} />
            </Col>
          </FormGroup>

          {/* Mã Giảm Giá */}
          <FormGroup row>
            <Label for="" sm={2}>
              Mã Giảm Giá:
            </Label>
            <Col sm={10}>
              <Input value={modalInputVoucher} readOnly={true} />
            </Col>
          </FormGroup>
          {/* Thành Tiền */}
          <FormGroup row>
            <Label for="" sm={2}>
              Thành Tiền:
            </Label>
            <Col sm={10}>
              <Input
                value={
                  modalInputVoucher !== ""
                    ? getTotalPrice(pizzaSize.thanhTien, percent)
                    : pizzaSize.thanhTien
                }
                readOnly={true}
              />
            </Col>
          </FormGroup>
          {/* Lời Nhắn */}
          <FormGroup row>
            <Label for="" sm={2}>
              Lời Nhắn:
            </Label>
            <Col sm={10}>
              <Input value={inputMessage} readOnly={true} />
            </Col>
          </FormGroup>
        </Modal.Body>
        <Modal.Footer>
          <Button
            className="fw-bold"
            color="danger"
            onClick={handleCloseModalOrder}
          >
            Close
          </Button>
          <Button color="warning" onClick={callApiCreateOrder}>
            Save Changes
          </Button>
        </Modal.Footer>
      </Modal>

      {/* Error Modal */}
      <Modal size="lg" show={showModalError} onHide={handleCloseModalError}>
        <Modal.Header className="bg-danger" closeButton>
          <Modal.Title
            className="text-light"
            id="contained-modal-title-vcenter"
          >
            Vui Lòng Kiểm Tra Lại Thông Tin
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <h4 className="fs-5">{errorMessage}</h4>
        </Modal.Body>
        <Modal.Footer>
          <Button
            color="danger"
            className="fw-bold"
            onClick={handleCloseModalError}
          >
            Close
          </Button>
        </Modal.Footer>
      </Modal>

      {/* Success Modal */}
      <Modal size="lg" show={showModalSuccess} onHide={resetData}>
        <Modal.Header className="bg-success" closeButton>
          <Modal.Title
            // className="text-dark"
            id="contained-modal-title-vcenter"
          >
            Mã Order của bạn:
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <h4 className="fs-5">{orderId}</h4>
        </Modal.Body>
        <Modal.Footer>
          <Button color="success" className="fw-bold" onClick={resetData}>
            Close
          </Button>
        </Modal.Footer>
      </Modal>
    </Container>
  );
}

export default SubmitButton;
