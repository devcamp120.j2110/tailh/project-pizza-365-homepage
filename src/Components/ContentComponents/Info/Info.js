function Info() {
  return (
    <div className="introduction">
      <div className="content-title d-flex justify-content-center">
        <h2 className="text-center">Tại sao lại Pizza 365</h2>
      </div>
      <div className="container row p-0 m-0">
        <div className="variety col-sm-3 p-3 bg-variety1">
          <h4 className="introduction-title">Đa dạng</h4>
          <p className="introduction-content">
            Số lượng pizza đa dạng, có đầy đủ các loại pizza đang hot nhất hiện
            nay
          </p>
        </div>
        <div className="variety col-sm-3 p-3 bg-variety2">
          <h4 className="introduction-title">Chất Lượng</h4>
          <p className="introduction-content">
            Số lượng pizza đa dạng, có đầy đủ các loại pizza đang hot nhất hiện
            nay
          </p>
        </div>
        <div className="variety col-sm-3 p-3 bg-variety3">
          <h4 className="introduction-title">Hương Vị</h4>
          <p className="introduction-content">
            Số lượng pizza đa dạng, có đầy đủ các loại pizza đang hot nhất hiện
            nay
          </p>
        </div>
        <div className="variety col-sm-3 p-3 bg-variety4">
          <h4 className="introduction-title">Dịch Vụ</h4>
          <p className="introduction-content">
            Số lượng pizza đa dạng, có đầy đủ các loại pizza đang hot nhất hiện
            nay
          </p>
        </div>
      </div>
    </div>
  );
}

export default Info;
