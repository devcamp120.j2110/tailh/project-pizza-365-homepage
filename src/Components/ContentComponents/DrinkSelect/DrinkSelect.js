import { useEffect, useState } from "react";

function DrinkSelect({ changeDrinkSelect }) {
  const fetchApi = async (paramUrl, paramOption = {}) => {
    const response = await fetch(paramUrl, paramOption);
    const responseData = await response.json();
    return responseData;
  };
  const [drinkSelectArr, setDrinkSelect] = useState([]);

  useEffect(() => {
    console.log("Get DrinkList");
    fetchApi(`http://42.115.221.44:8080/devcamp-pizza365/drinks`)
      .then((data) => {
        console.log(data);
        setDrinkSelect(data);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  const valueDrinkSelect = (e) => {
    var drinkSelectedValue = e.target.value; //Lấy value từ select
    var drinkSelectOptions = e.target.options; 
    var drinkSelectedText = drinkSelectOptions[drinkSelectOptions.selectedIndex].innerText; //Lấy text từ select
    // Thay đổi state DrinkSelect
    changeDrinkSelect({
      drinkValue: drinkSelectedValue,
      drinkText: drinkSelectedText,
    });
  };

  return (
    <div className="container drink">
      <div className="content-title d-flex justify-content-center">
        <h2 className="text-center">Chọn Đồ Uống</h2>
      </div>
      <form>
        <div className="form-group">
          <select
            onChange={valueDrinkSelect}
            className="form-control"
            id="select-drink"
          >
            <option value="" selected>
              Tất cả loại nước uống
            </option>
            {drinkSelectArr.map((drink) => {
              return (
                <option value={drink.maNuocUong} key={drink.maNuocUong}>
                  {drink.tenNuocUong}
                </option>
              );
            })}
          </select>
        </div>
      </form>
    </div>
  );
}

export default DrinkSelect;
