function FormSubmit({
  inputName,
  changeInputName,
  inputEmail,
  changeInputEmail,
  inputPhone,
  changeInputPhone,
  inputAddress,
  changeInputAddress,
  inputVoucher,
  changeInputVoucher,
  inputMessage,
  changeInputMessage,
}) {
  //Input Name
  const onChangeInputName = (e) => {
    var valueInputName = onChangeInput(e);
    changeInputName(valueInputName);
  };

  //Input Email
  const onChangeInputEmail = (e) => {
    var valueInputEmail = onChangeInput(e);
    changeInputEmail(valueInputEmail);
  };

  //Input Phone Number
  const onChangeInputPhone = (e) => {
    var valueInputPhone = onChangeInput(e);
    changeInputPhone(valueInputPhone);
  };

  //Input Address
  const onChangeInputAddress = (e) => {
    var valueInputAddress = onChangeInput(e);
    changeInputAddress(valueInputAddress);
  };

  //Input Voucher
  const onChangeInputVoucher = (e) => {
    var valueInputVoucher = onChangeInput(e);
    changeInputVoucher(valueInputVoucher);
  };

  // //Input Message
  const onChangeInputMessage = (e) => {
    var valueInputMessage = onChangeInput(e);
    changeInputMessage(valueInputMessage);
  };

  //Hàm lấy dữ liệu thay đổi từ input
  const onChangeInput = (e) => {
    var valueInput = e.target.value;
    return valueInput;
  };

  return (
    <div className="container user-form">
      <div className="content-title d-flex justify-content-center">
        <h2 className="text-center">Gửi Đơn Hàng</h2>
      </div>
      <form className="pt-2">
        <div className="form-group">
          <label>Tên</label>
          <input
            className="form-control"
            id="inp-name"
            placeholder="Nhập tên"
            onChange={onChangeInputName}
            value={inputName}
          ></input>
        </div>
        <div className="form-group">
          <label>Email</label>
          <input
            className="form-control"
            id="inp-email"
            placeholder="Nhập email"
            onChange={onChangeInputEmail}
            value={inputEmail}
          ></input>
        </div>
        <div className="form-group">
          <label>Số điện thoại</label>
          <input
            className="form-control"
            id="inp-phone"
            placeholder="Nhập số điện thoại"
            onChange={onChangeInputPhone}
            value={inputPhone}
          ></input>
        </div>
        <div className="form-group">
          <label>Địa chỉ</label>
          <input
            className="form-control"
            id="inp-address"
            placeholder="Nhập địa chỉ"
            onChange={onChangeInputAddress}
            value={inputAddress}
          ></input>
        </div>
        <div className="form-group">
          <label>Mã giảm giá</label>
          <input
            className="form-control"
            id="inp-voucher"
            placeholder="Nhập mã giảm giá"
            onChange={onChangeInputVoucher}
            value={inputVoucher}
          ></input>
        </div>
        <div className="form-group">
          <label>Lời nhắn</label>
          <input
            className="form-control"
            id="inp-message"
            placeholder="Nhập lời nhắn"
            onChange={onChangeInputMessage}
            value={inputMessage}
          ></input>
        </div>
      </form>
    </div>
  );
}

export default FormSubmit;
