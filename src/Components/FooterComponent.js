import {
  FaFacebook,
  FaInstagramSquare,
  FaSnapchatGhost,
  FaPinterestP,
  FaTwitterSquare,
  FaLinkedin,
} from "react-icons/fa";

function FooterComponent() {
  return (
    <div className="container-fluid bg-main-color p-5">
      <div className="row text-center">
        <div className="col-sm-12">
          <h4 className="m-2">Footer</h4>
          <a href="#navbarNav" className="btn btn-dark m-3">
            <i className="fa fa-arrow-up"></i>To the top
          </a>
          <div className="m-2">
            <FaFacebook />
            <FaInstagramSquare />
            <FaSnapchatGhost />
            <FaPinterestP />
            <FaTwitterSquare />
            <FaLinkedin />
          </div>
          <p className="m-2 fw-bold">Powered by DEVCAMP</p>
        </div>
      </div>
    </div>
  );
}

export default FooterComponent;
