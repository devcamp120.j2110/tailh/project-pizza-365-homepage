
function HeaderComponent() {
    return(
        <div className="container-fluid" style={{ padding: "0" }}>
        <nav className="navbar navbar-expand-lg navbar-light fixed-top">
          <button
            className="navbar-toggler"
            type="button"
            data-toggle="collapse"
            data-target="#navbarNav"
            aria-controls="navbarNav"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="row collapse navbar-collapse " id="navbarNav">
            <ul className="navbar-nav row" style={{ width: "100%" }}>
              <li className="nav-item text-center col-sm-3">
                <a className="nav-link active" href="#navbarNav">
                  Trang chủ
                </a>
              </li>
              <li className="nav-item text-center col-sm-3">
                <a className="nav-link" href="#navbarNav">
                  Combo
                </a>
              </li>
              <li className="nav-item text-center col-sm-3">
                <a className="nav-link" href="#navbarNav">
                  Loại Pizza
                </a>
              </li>
              <li className="nav-item text-center col-sm-3">
                <a className="nav-link" href="#navbarNav">
                  Gửi Đơn Hàng
                </a>
              </li>
            </ul>
          </div>
        </nav>
      </div>
    )
}

export default HeaderComponent